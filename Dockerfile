FROM anapsix/alpine-java:8_server-jre
MAINTAINER Michal Buczko "michal.buczko@gmail.com"

RUN apk --no-cache --update upgrade
RUN apk add --update ca-certificates openssl && apk add tini
RUN wget https://github.com/riemann/riemann/releases/download/0.2.12/riemann-0.2.12.tar.bz2
RUN tar xjvf riemann-0.2.12.tar.bz2 && mv riemann-0.2.12 /opt/riemann && rm riemann-0.2.12.tar.bz2

# Expose the ports for inbound events and websockets
EXPOSE 5555
EXPOSE 5555/udp
EXPOSE 5556

WORKDIR /opt/riemann/bin
ENTRYPOINT ["/sbin/tini", "--"]

CMD ["/opt/riemann/bin/riemann", "-a", "start"]
